'''
Created on 07/10/2020

@author: rafael
'''

class FactorialRecursivo:
    
    def calculoFactorial(self,numero):
        if(numero<=1):
            return 1
        else:
            return numero*FactorialRecursivo.calculoFactorial(self, numero-1)
            
        pass

a=FactorialRecursivo()
try:
    numero=int(input("Ingresa el numero para obtener su factorial: "))
    print("Su factorial es: ",a.calculoFactorial(numero))
except ValueError:
    print("No ingreaste un valor numerico entero")